package com.example.somemessenger.messageRecyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.somemessenger.R

class MessageAdapter(val listener: Listener): RecyclerView.Adapter<MessageAdapter.MessageHolder>(){
    val messageList = ArrayList<Message>()

    class MessageHolder(item: View) : RecyclerView.ViewHolder(item){
        var tvId: TextView = item.findViewById(R.id.tvId)
        var tvTitle: TextView = item.findViewById(R.id.tvTitle)
        fun bind(mess: Message, listener: Listener){
            tvId.setText(mess.id.toString())
            tvTitle.setText(mess.title)
            itemView.setOnClickListener {
                listener.onClick(mess)
            }
        }
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false)
        return MessageHolder(view)
    }

    override fun onBindViewHolder(holder: MessageHolder, position: Int) {
        holder.bind(messageList[position], listener)
    }

    fun addMessage(mess: Message){
        messageList.add(mess)
        notifyDataSetChanged()
    }

    fun clear(){
        messageList.clear()
        notifyDataSetChanged()
    }

    interface Listener{
        fun onClick(mess: Message)
    }

}