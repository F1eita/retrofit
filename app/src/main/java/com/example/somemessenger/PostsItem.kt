package com.example.somemessenger

data class PostsItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)