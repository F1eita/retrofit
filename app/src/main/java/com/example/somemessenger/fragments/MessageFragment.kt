package com.example.somemessenger.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.somemessenger.APIInterface
import com.example.somemessenger.MessViewModel
import com.example.somemessenger.PostsItem
import com.example.somemessenger.R
import com.example.somemessenger.messageRecyclerView.Message
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.concurrent.fixedRateTimer
import kotlin.properties.Delegates

class MessageFragment : Fragment() {
    lateinit var bodyText: String
    lateinit var message: Message
    val viewModel: MessViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_message, container, false)

        val tvTitle: TextView = fragmentLayout.findViewById(R.id.tvTitleMess)
        val edMess: EditText = fragmentLayout.findViewById(R.id.edMessage)
        val btnSave: Button = fragmentLayout.findViewById(R.id.btnSave)
        viewModel.mess.observe(requireActivity(), Observer {
            message = it
            tvTitle.setText(it.title)
            edMess.setText(it.body)
        })
        btnSave.setOnClickListener {
            bodyText = edMess.getText().toString()
            message.body = bodyText
            postData()
        }
        return fragmentLayout
    }


    companion object {
        @JvmStatic
        fun newInstance() = MessageFragment()
    }
    fun postData(){
        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .build()

        val service = retrofitBuilder.create(APIInterface::class.java)

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val postsItem = PostsItem(message.body, message.id, message.title, message.userId)
            val response = service.pushPost(postsItem)

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Toast.makeText(requireActivity(), "Success!", Toast.LENGTH_SHORT).show()
                } else {

                    Log.e("RETROFIT_ERROR", response.code().toString())

                }
            }
        }
    }
}