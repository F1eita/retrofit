package com.example.somemessenger.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.somemessenger.*
import com.example.somemessenger.messageRecyclerView.Message
import com.example.somemessenger.messageRecyclerView.MessageAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://jsonplaceholder.typicode.com/"
class MessageListFragment : Fragment(), MessageAdapter.Listener {

    val adapter = MessageAdapter(this)
    lateinit var navController: NavController
    lateinit var rcView: RecyclerView

    val viewModel: MessViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_message_list, container, false)
        navController = NavHostFragment.findNavController(this)
        rcView = fragmentLayout.findViewById(R.id.recViewMess)
        rcView.layoutManager = LinearLayoutManager(this.context)
        rcView.adapter = adapter
        return fragmentLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.clear()
        getData()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            MessageListFragment()
    }
    override fun onClick(mess: Message) {
        viewModel.setCurrentMess(mess)
        navController.navigate(R.id.messageFragment)
    }

    fun getData(){
        val retrofitBuilder = Retrofit.Builder().
                addConverterFactory(GsonConverterFactory.create()).
                baseUrl(BASE_URL).build().
                create(APIInterface::class.java)
        val retrofitData = retrofitBuilder.getData()
        retrofitData.enqueue(object: Callback<List<PostsItem>?>{
            override fun onResponse(
                call: Call<List<PostsItem>?>,
                response: Response<List<PostsItem>?>
            ) {
                val responseBody =  response.body()!!
                for (Posts in responseBody){
                    val message = Message(Posts.userId, Posts.id, Posts.title, Posts.body)
                    adapter.addMessage(message)
                }
            }

            override fun onFailure(call: Call<List<PostsItem>?>, t: Throwable) {
                Log.d("MessListFragment", "onFailure: " + t.message)
            }
        })
    }


}