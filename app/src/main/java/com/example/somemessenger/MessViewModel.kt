package com.example.somemessenger

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.somemessenger.messageRecyclerView.Message

class MessViewModel: ViewModel() {

    private val _mess = MutableLiveData<Message>(Message(0, 0,"", ""))
    val mess: LiveData<Message> = _mess
    fun setCurrentMess(mess: Message){
        _mess.value = mess
    }

}